# <!-- coding: utf-8 -->
#
# quelques fonctions pour le MONiR
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
mga  <- function() {
  source("geo/scripts/monir.R");
}
#
# les variables globales
Drive <- substr( getwd(),1,2)
baseDir <- sprintf("%s/web", Drive)
varDir <- sprintf("%s/bvi35/CouchesMONIR", Drive);
texDir <- sprintf("%s/web/geo/%s", Drive, "MONIR")
dir.create(varDir, showWarnings = FALSE, recursive = TRUE)
dir.create(texDir, showWarnings = FALSE, recursive = TRUE)
ignDir <- sprintf("%s/bvi35/CouchesIGN", Drive);
webDir <- sprintf("%s/web.heb/bv/monir", Drive);
#
# les bibliothèques
setwd(baseDir)
source("geo/scripts/mga.R");
source("geo/scripts/misc.R");
source("geo/scripts/misc_couches.R");
source("geo/scripts/misc_fonds.R");
source("geo/scripts/misc_gdal.R");
source("geo/scripts/misc_tex.R");
source("geo/scripts/monir_cartes.R");
source("geo/scripts/monir_couches.R");
source("geo/scripts/monir_ocs.R");
source("geo/scripts/monir_parcours.R");

#
# les commandes permettant le lancement
DEBUG <- FALSE
if ( interactive() ) {
  carp("interactive")
# un peu de nettoyage
  graphics.off()
  par(mar=c(0,0,0,0), oma=c(0,0,0,0))
  options(stringsAsFactors=FALSE)
} else {
  carp("console")
}
